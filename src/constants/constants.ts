export const LEFT_MOUSE_CLICK_VALUE = 0;
export const RIGHT_MOUSE_CLICK_VALUE = 2;
export const NUMBER_OF_SHRINKS = 10;
export const SHRINK_TIME_MILLIS = 3000;
export const INFINITY = 1e10;
