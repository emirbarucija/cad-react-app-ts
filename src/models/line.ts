import { Point } from "./point";

export class Line {
    firstPoint: Point;
    secondPoint: Point;
    constructor(firstPoint: Point, secondPoind: Point) {
        this.firstPoint = firstPoint;
        this.secondPoint = secondPoind;
    }
}
