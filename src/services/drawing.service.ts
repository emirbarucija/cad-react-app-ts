import { Context } from "vm";
import { Props } from "../components/canvas";
import { INFINITY } from "../constants/constants";
import { Line } from "../models/line";
import { Point } from "../models/point";

export class DrawingService {
    context!: Context;

    public setContext(context: Context): void {
        this.context = context;
    }

    public clearAndRedrawCanvas(props: Props): void {
        this.clearCanvas();
        this.drawLines(props.allLines);
        this.drawPoints(props.allPoints);
    }

    public clearCanvas(): void {
        this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
    }

    public drawLines(allLines: Line[]): void {
        for (let i = 0; i < allLines.length; i++) {
            this.drawLine(allLines[i]);
        }
    }

    public drawLine(line: Line): void {
        this.context.strokeStyle = 'black';
        this.context.beginPath();
        this.context.moveTo(line.firstPoint.x, line.firstPoint.y);
        this.context.lineTo(line.secondPoint.x, line.secondPoint.y);
        this.context.lineWidth = 1;
        this.context.stroke();
    }

    public drawPoints(allPoints: Point[][]): void {
        for (let i = 0; i < allPoints.length; i++) {
            for (let j = 0; j < allPoints[i].length; j++) {
                this.drawPoint(allPoints[i][j]);
            }
        }
    }

    public drawPoint(point: Point): void {
        this.context.strokeStyle = 'red';
        this.context.beginPath();
        this.context.arc(point.x, point.y, 1, 0, 2 * Math.PI, true);
        this.context.stroke();
    }

    public drawAndReturnPointIfLinesIntersect(lineOne: Line, lineTwo: Line): Point {
        let kOne = this.calculateK(lineOne);
        let kTwo = this.calculateK(lineTwo);
        let x, y: number;
        if (kOne == kTwo) {
            // If coefficients k are equal, lines are parallel and intersection point is at infinity
            x = INFINITY;
            y = INFINITY;
        } else if (lineOne.firstPoint.x == lineOne.secondPoint.x) {
            // If first line is vertical
            x = lineOne.firstPoint.x;
            y = this.calculateYIntersection(x, kOne, lineOne);
        } else if (lineTwo.firstPoint.x == lineTwo.secondPoint.x) {
            // If second line is vertical
            x = lineTwo.firstPoint.x;
            y = this.calculateYIntersection(x, kOne, lineOne);
        } else if (lineOne.firstPoint.y == lineOne.secondPoint.y) {
            // If first line is horizontal
            x = this.calculateXIntersection(kOne, lineOne, kTwo, lineTwo);
            y = lineOne.firstPoint.y;
        } else if (lineTwo.firstPoint.y == lineTwo.secondPoint.y) {
            // If second line is horizontal
            x = this.calculateXIntersection(kOne, lineOne, kTwo, lineTwo);
            y = lineTwo.firstPoint.y;
        } else {
            x = this.calculateXIntersection(kOne, lineOne, kTwo, lineTwo);
            y = this.calculateYIntersection(x, kOne, lineOne);
        }
        let point = new Point(x, y);

        if (this.pointBelongsToBothLines(point, lineOne, lineTwo)) {
            this.drawPoint(point);
            return point;
        }
        return null as unknown as Point;
    }

    private calculateK(line: Line): number {
        // If x coordinates are the same, k is infinite
        if (line.secondPoint.x == line.firstPoint.x) {
            return INFINITY;
        } else {
            return (line.secondPoint.y - line.firstPoint.y) / (line.secondPoint.x - line.firstPoint.x);
        }
    }

    private pointBelongsToBothLines(point: Point, lineOne: Line, lineTwo: Line): boolean {
        return point.x <= Math.max(lineOne.secondPoint.x, lineOne.firstPoint.x) && point.x >= Math.min(lineOne.firstPoint.x, lineOne.secondPoint.x) &&
            point.y <= Math.max(lineOne.secondPoint.y, lineOne.firstPoint.y) && point.y >= Math.min(lineOne.firstPoint.y, lineOne.secondPoint.y) &&
            point.x <= Math.max(lineTwo.secondPoint.x, lineTwo.firstPoint.x) && point.x >= Math.min(lineTwo.firstPoint.x, lineTwo.secondPoint.x) &&
            point.y <= Math.max(lineTwo.secondPoint.y, lineTwo.firstPoint.y) && point.y >= Math.min(lineTwo.firstPoint.y, lineTwo.secondPoint.y);
    }

    private calculateXIntersection(kOne: number, lineOne: Line, kTwo: number, lineTwo: Line): number {
        return (kOne * lineOne.firstPoint.x - kTwo * lineTwo.firstPoint.x - lineOne.firstPoint.y + lineTwo.firstPoint.y) / (kOne - kTwo);
    }

    private calculateYIntersection(x: number, kOne: number, lineOne: Line): number {
        return kOne * x - kOne * lineOne.firstPoint.x + lineOne.firstPoint.y;
    }
}
