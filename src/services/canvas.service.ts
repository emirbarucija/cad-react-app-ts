import { ReactInstance, RefObject } from 'react';
import ReactDOM from 'react-dom';
import { Context } from 'vm';
import { Point } from '../models/point';

export class CanvasService {
    canvas!: HTMLCanvasElement;

    public getCanvas(): HTMLCanvasElement {
        return this.canvas;
    }

    public setCanvas(refs: { [x: string]: ReactInstance; canvas?: any; }): void {
        this.canvas = ReactDOM.findDOMNode(refs.canvas) as HTMLCanvasElement;
    }

    public getContext(): Context {
        return this.canvas.getContext("2d") as Context
    }

    public getPoint(clientX: number, clientY: number): Point {
        const rect = this.canvas.getBoundingClientRect();
        let startX = clientX - rect.x;
        let startY = clientY - rect.y;
        let x = startX * this.canvas.width / rect.width;
        let y = startY * this.canvas.height / rect.height;

        return new Point(x, y);
    }
}
