import { NUMBER_OF_SHRINKS } from "../constants/constants";
import { Line } from "../models/line";
import { Point } from "../models/point";
import { DrawingService } from "./drawing.service";

export class ShrinkingService {
    drawingService: DrawingService;

    constructor(drawingService: DrawingService) {
        this.drawingService = drawingService;
    }

    public getIntersectionPointsForCurrentLine(allLines: Line[], currentLine: Line): Point[] {
        let currentPoints: Point[] = [];

        for (let i = 0; i < allLines.length; i++) {
            let point = this.drawingService.drawAndReturnPointIfLinesIntersect(allLines[i], currentLine);
            if (point) currentPoints.push(point);
        }

        return currentPoints;
    }

    public shrinkLines(shrinkCounter: number, allLines: Line[]) {
        this.drawingService.clearCanvas();

        for (let i = 0; i < allLines.length; i++) {
            let line = this.getShrunkLine(allLines[i], shrinkCounter);
            this.drawingService.drawLine(line);
        }
    }

    private getShrunkLine(originalLine: Line, shrinkCounter: number): Line {
        let stepX = this.calculateStepX(originalLine);
        let firstX, secondX;
        if (originalLine.firstPoint.x < originalLine.secondPoint.x) {
            firstX = this.moveCoordinateRightOrDown(originalLine.firstPoint.x, shrinkCounter, stepX);
            secondX = this.moveCoordinateLeftOrUp(originalLine.secondPoint.x, shrinkCounter, stepX);
        } else {
            firstX = this.moveCoordinateLeftOrUp(originalLine.firstPoint.x, shrinkCounter, stepX);
            secondX = this.moveCoordinateRightOrDown(originalLine.secondPoint.x, shrinkCounter, stepX);
        }

        let stepY = this.calculateStepY(originalLine);
        let firstY, secondY;
        if (originalLine.firstPoint.y < originalLine.secondPoint.y) {
            firstY = this.moveCoordinateRightOrDown(originalLine.firstPoint.y, shrinkCounter, stepY);
            secondY = this.moveCoordinateLeftOrUp(originalLine.secondPoint.y, shrinkCounter, stepY);
        } else {
            firstY = this.moveCoordinateLeftOrUp(originalLine.firstPoint.y, shrinkCounter, stepY);
            secondY = this.moveCoordinateRightOrDown(originalLine.secondPoint.y, shrinkCounter, stepY);
        }

        return new Line(new Point(firstX, firstY), new Point(secondX, secondY));
    }

    private calculateStepX(line: Line): number {
        return this.calculateStep(line.firstPoint.x, line.secondPoint.x);
    }

    private calculateStepY(line: Line): number {
        return this.calculateStep(line.firstPoint.y, line.secondPoint.y);
    }

    private calculateStep(coordinate1: number, coordinate2: number): number {
        return Math.abs(coordinate1 - coordinate2) / (2 * NUMBER_OF_SHRINKS);
    }

    private moveCoordinateLeftOrUp(coordinate: number, shrinkCounter: number, step: number) {
        return coordinate - shrinkCounter * step;
    }

    private moveCoordinateRightOrDown(coordinate: number, shrinkCounter: number, step: number) {
        return coordinate + shrinkCounter * step;
    }
}