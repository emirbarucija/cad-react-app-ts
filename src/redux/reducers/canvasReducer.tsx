import {
  DELETE_CURRENT_LINE,
  START_NEW_LINE,
  SAVE_NEW_LINE,
  DRAW_NEW_LINE,
  REMOVE_ALL_LINES_AND_POINTS,
  ActionTypes
} from '../actionTypes';

const canvasReducerDefaultState: any = {
  shouldDrawLine: false,
  allLines: [],
  allPoints: []
}

const canvasReducer = (state = canvasReducerDefaultState, action: ActionTypes) => {
  switch (action.type) {
    case START_NEW_LINE:
      return {
        ...state,
        currentLine: { firstPoint: action.firstPoint },
        shouldDrawLine: true
      };
    case DRAW_NEW_LINE: {
      return {
        ...state,
        currentLine: { ...state.currentLine, secondPoint: action.secondPoint },
        currentPoints: action.points
      };
    }
    case SAVE_NEW_LINE:
      return {
        ...state,
        shouldDrawLine: false,
        allLines: [...state.allLines, state.currentLine],
        allPoints: [...state.allPoints, state.currentPoints],
        currentLine: null,
        currentPoints: null
      };
    case DELETE_CURRENT_LINE:
      return {
        ...state,
        currentLine: null,
        currentPoints: null,
        shouldDrawLine: false,
      };
    case REMOVE_ALL_LINES_AND_POINTS:
      return {
        ...state,
        currentLine: null,
        currentPoints: null,
        shouldDrawLine: false,
        allLines: [],
        allPoints: []
      };
    default:
      return state;
  }
};

export { canvasReducer };
