import { Line } from "../models/line";
import { Point } from "../models/point";
import { AppState } from "./store";

export const getCanvasState = (store: AppState) => store.cad;

export const getAllLines = (store: AppState): Line[] =>
  getCanvasState(store) ? getCanvasState(store).allLines : [];

export const getShouldDrawLine = (store: AppState): boolean =>
  getCanvasState(store) ? getCanvasState(store).shouldDrawLine : true;

export const getCurrentLine = (store: AppState): Line =>
  getCanvasState(store) ? getCanvasState(store).currentLine : null as unknown as Line;

export const getFirstPoint = (store: AppState): Point =>
  getCurrentLine(store) ? getCurrentLine(store).firstPoint : null as unknown as Point;

export const getAllPoints = (store: AppState): Point[][] =>
  getCanvasState(store) ? getCanvasState(store).allPoints : null;

export const getCurrentPoints = (store: AppState): Point[] =>
  getCanvasState(store) ? getCanvasState(store).currentPoints : null;
