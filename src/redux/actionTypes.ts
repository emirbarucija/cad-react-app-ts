import { Point } from "../models/point";

export const START_NEW_LINE = 'START_NEW_LINE'; //LEFT MOUSE DOWN
export const DRAW_NEW_LINE = 'DRAW_NEW_LINE'; //MOUSE_MOVE
export const SAVE_NEW_LINE = 'SAVE_NEW_LINE'; //LEFT MOUSE UP
export const DELETE_CURRENT_LINE = 'DELETE_CURRENT_LINE'; //RIGHT MOUSE DOWN
export const REMOVE_ALL_LINES_AND_POINTS = 'REMOVE_ALL_LINES_AND_POINTS';

export interface startNewLineAction {
    type: typeof START_NEW_LINE;
    firstPoint: Point;
};

export interface drawNewLineAction {
    type: typeof DRAW_NEW_LINE;
    secondPoint: Point;
    points: Point[][];
};

export interface saveNewLineAction {
    type: typeof SAVE_NEW_LINE;
};

export interface deleteNewLineAction {
    type: typeof DELETE_CURRENT_LINE;
};

export interface removeAllLinesAndPointsAction {
    type: typeof REMOVE_ALL_LINES_AND_POINTS;
};

export type ActionTypes = startNewLineAction |
    drawNewLineAction |
    saveNewLineAction |
    deleteNewLineAction |
    removeAllLinesAndPointsAction;
