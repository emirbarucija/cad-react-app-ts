import {
    START_NEW_LINE,
    DRAW_NEW_LINE,
    SAVE_NEW_LINE,
    DELETE_CURRENT_LINE,
    REMOVE_ALL_LINES_AND_POINTS,
    ActionTypes
} from './actionTypes';
import { AppState } from './store';
import { Dispatch } from "redux";
import { Point } from '../models/point';

export const startNewLineAction = (firstPoint: Point): ActionTypes => ({
    type: START_NEW_LINE,
    firstPoint
});

export const drawNewLineAction = (secondPoint: Point, points: Point[][]): ActionTypes => ({
    type: DRAW_NEW_LINE,
    secondPoint,
    points
});

export const saveNewLineAction = (): ActionTypes => ({
    type: SAVE_NEW_LINE
});

export const deleteNewLineAction = (): ActionTypes => ({
    type: DELETE_CURRENT_LINE
});

export const removeAllLinesAndPointsAction = (): ActionTypes => ({
    type: REMOVE_ALL_LINES_AND_POINTS
});

export const startNewLine = (firstPoint: Point) => {
    return (dispatch: Dispatch<ActionTypes>, getState: () => AppState) => {
        dispatch(startNewLineAction(firstPoint));
    };
};

export const drawNewLine = (secondPoint: Point, points: Point[][]) => {
    return (dispatch: Dispatch<ActionTypes>, getState: () => AppState) => {
        dispatch(drawNewLineAction(secondPoint, points));
    };
};

export const saveNewLine = () => {
    return (dispatch: Dispatch<ActionTypes>, getState: () => AppState) => {
        dispatch(saveNewLineAction());
    };
};

export const deleteNewLine = () => {
    return (dispatch: Dispatch<ActionTypes>, getState: () => AppState) => {
        dispatch(deleteNewLineAction());
    };
};

export const removeAllLinesAndPoints = () => {
    return (dispatch: Dispatch<ActionTypes>, getState: () => AppState) => {
        dispatch(removeAllLinesAndPointsAction());
    };
};
