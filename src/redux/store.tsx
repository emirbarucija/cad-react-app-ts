import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk, { ThunkMiddleware } from 'redux-thunk';
import { ActionTypes } from './actionTypes';
import { canvasReducer } from './reducers/canvasReducer';

export const rootReducer = combineReducers({
  cad: canvasReducer
});

export type AppState = ReturnType<typeof rootReducer>;

export const store = createStore(
  rootReducer,
  applyMiddleware(thunk as ThunkMiddleware<AppState, ActionTypes>)
);
