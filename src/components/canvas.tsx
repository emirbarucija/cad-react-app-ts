import React, { ReactComponentElement, SyntheticEvent } from 'react';
import { connect } from 'react-redux';
import './canvas.css';
import { LEFT_MOUSE_CLICK_VALUE, NUMBER_OF_SHRINKS, RIGHT_MOUSE_CLICK_VALUE, SHRINK_TIME_MILLIS } from '../constants/constants';
import { getFirstPoint, getAllLines, getCurrentLine, getCurrentPoints, getAllPoints, getShouldDrawLine } from '../redux/selectors';
import { startNewLine, drawNewLine, saveNewLine, deleteNewLine, removeAllLinesAndPoints } from '../redux/action';
import { bindActionCreators } from 'redux';
import { AppState } from '../redux/store';
import { ActionTypes } from '../redux/actionTypes';
import { ThunkDispatch } from 'redux-thunk';
import { CanvasService } from '../services/canvas.service';
import { Line } from '../models/line';
import { DrawingService } from '../services/drawing.service';
import { ShrinkingService } from '../services/shrinking.service';
import { Point } from '../models/point';


export type Props = LinkStateProps & LinkDispatchProps;

export class Canvas extends React.Component<Props> {
  canvasService: CanvasService;
  drawingService: DrawingService;
  shrinkingService: ShrinkingService;

  constructor(props: Props) {
    super(props);
    this.canvasService = new CanvasService();
    this.drawingService = new DrawingService();
    this.shrinkingService = new ShrinkingService(this.drawingService);
  }

  public componentDidMount(): void {
    this.canvasService.setCanvas(this.refs);
    this.drawingService.setContext(this.canvasService.getContext());
  }

  //START NEW LINE
  private onLeftClickEvent(event: React.MouseEvent<HTMLElement>): void {
    let currentPoint = this.canvasService.getPoint(event.clientX, event.clientY);

    if (this.props.shouldDrawLine) {
      this.props.onSecondLeftClick();
    } else {
      this.props.onFirstLeftClick(currentPoint);
    }
  }

  // DRAW_NEW_LINE
  private onMouseMoveEvent(event: React.MouseEvent<HTMLElement>): void {
    if (this.props.shouldDrawLine) {
      let currentPointOfLine = this.canvasService.getPoint(event.clientX, event.clientY);

      this.drawingService.clearAndRedrawCanvas(this.props);
      let currentLine = new Line(this.props.firstPoint, currentPointOfLine);
      this.drawingService.drawLine(currentLine);

      let currentPoints = this.shrinkingService.getIntersectionPointsForCurrentLine(this.props.allLines, currentLine);

      this.props.onMouseMove(currentPointOfLine, currentPoints);
    }
  }

  // DELETE_CURRENT_LINE
  private onRightClickEvent(event: React.MouseEvent<HTMLElement>): void {
    this.drawingService.clearAndRedrawCanvas(this.props);

    this.props.onRightClick();
  }

  private onCollapseLinesClick(): void {
    let allLines = this.props.allLines;

    for (let shrinkCounter = 1; shrinkCounter <= NUMBER_OF_SHRINKS; shrinkCounter++) {
      setTimeout(() => {
        this.shrinkingService.shrinkLines(shrinkCounter, allLines);
      }, shrinkCounter * SHRINK_TIME_MILLIS / NUMBER_OF_SHRINKS);
    }

    this.props.onCollapseLines();
  }

  public render() {
    const onMouseDownClick = (event: any) => {
      let nativeEvent = event.nativeEvent;
      if (nativeEvent.button == LEFT_MOUSE_CLICK_VALUE) {
        this.onLeftClickEvent(nativeEvent);
      } else if (nativeEvent.button == RIGHT_MOUSE_CLICK_VALUE) {
        this.onRightClickEvent(nativeEvent);
      }
    }

    const onMouseMoveEvent = (event: any) => {
      let nativeEvent = event.nativeEvent;
      this.onMouseMoveEvent(nativeEvent);
    }

    const collapseLines = () => {
      this.onCollapseLinesClick();
    };

    const buttonText = "collapse lines";

    return (
      <div className="row">
        <div>
          <canvas id="canvas" ref="canvas"
            onMouseDown={onMouseDownClick}
            onMouseMove={onMouseMoveEvent}
          />
        </div>
        <div>
          <button className="collapse-lines" onClick={collapseLines}>{buttonText}</button>
        </div>
      </div>
    );
  }
}

interface LinkStateProps {
  shouldDrawLine: boolean;
  firstPoint: Point;
  allLines: Line[];
  currentLine: Line;
  currentPoints: Point[];
  allPoints: Point[][];
}
interface LinkDispatchProps {
  onFirstLeftClick: (firstPoint: Point) => void;
  onSecondLeftClick: () => void;
  onMouseMove: (secondPoint: Point, points: any) => void;
  onRightClick: () => void;
  onCollapseLines: () => void;
}

const mapStateToProps = (
  state: AppState
): LinkStateProps => ({
  shouldDrawLine: getShouldDrawLine(state),
  firstPoint: getFirstPoint(state),
  allLines: getAllLines(state),
  currentLine: getCurrentLine(state),
  currentPoints: getCurrentPoints(state),
  allPoints: getAllPoints(state)
});

const mapDispatchToProps = (
  dispatch: ThunkDispatch<any, any, ActionTypes>
): LinkDispatchProps => ({
  onFirstLeftClick: bindActionCreators(startNewLine, dispatch),
  onSecondLeftClick: bindActionCreators(saveNewLine, dispatch),
  onMouseMove: bindActionCreators(drawNewLine, dispatch),
  onRightClick: bindActionCreators(deleteNewLine, dispatch),
  onCollapseLines: bindActionCreators(removeAllLinesAndPoints, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Canvas);
