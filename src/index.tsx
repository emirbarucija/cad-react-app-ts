import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { store } from './redux/store';
import Canvas from './components/canvas';
import { Provider } from 'react-redux';

ReactDOM.render((
  <Provider store={store}>
    <Canvas />
  </Provider>

), document.getElementById('root'));
