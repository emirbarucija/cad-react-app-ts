# How to use the project

After cloning, run:

### `npm i`

to install all dependencies to node_modules folder.

After that, run:

### `npm start`

to start the application. The app will be started on [http://localhost:3000](http://localhost:3000).
